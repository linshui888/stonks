package fr.lezoo.stonks.manager;

import fr.lezoo.stonks.Stonks;
import fr.lezoo.stonks.stock.Stock;
import fr.lezoo.stonks.share.Share;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class DividendManager {

    /**
     * The shortest dividends period is one day. This time out
     * is half a day because we can't be sure the runnable will be
     * executed at the very exact time.
     * <p>
     * This is arbitrarily set at 1 hour which is much longer than
     * the dividends runnable period.
     */
    private static final long DIVIDEND_SAFE_TIME_OUT = 1000 * 60 * 60;

    public void load() {

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime targetTime = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), Stonks.plugin.configManager.dividendsRedeemHour, 0);

        long delay = targetTime.toEpochSecond(ZoneOffset.UTC) - now.toEpochSecond(ZoneOffset.UTC);
        if (delay < 0) {
            delay += 86400; // add 24 hours in seconds
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                // code to be executed every 24 hours at 6 p.m.
                checkForDividends();
            }
        }.runTaskTimer(Stonks.plugin, delay, 86400);
    }

    public void checkForDividends() {
        // Loop through stocks with dividends support
        for (Stock stock : Stonks.plugin.stockManager.getStocks())
            if (stock.hasDividends() && stock.getDividends().canGiveDividends()) {

                // Apply dividends cooldown
                stock.getDividends().setLastApplication(System.currentTimeMillis() - DIVIDEND_SAFE_TIME_OUT);

                // Give money to shares
                for (Share share : Stonks.plugin.shareManager.getByStock(stock))
                    share.addToWallet(stock.getDividends().applyFormula(share));
            }
    }
}
