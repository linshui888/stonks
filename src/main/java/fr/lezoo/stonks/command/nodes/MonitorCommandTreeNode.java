package fr.lezoo.stonks.command.nodes;

import fr.lezoo.stonks.Stonks;
import fr.lezoo.stonks.command.objects.CommandTreeNode;
import fr.lezoo.stonks.command.objects.parameter.Parameter;
import fr.lezoo.stonks.player.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MonitorCommandTreeNode extends CommandTreeNode {
    public MonitorCommandTreeNode(CommandTreeNode parent) {
        super(parent, "monitor");
        addParameter(Parameter.PLAYER);
    }

    @Override
    public CommandResult execute(CommandSender sender, String[] args) {
        if (args.length < 2)
            return CommandResult.FAILURE;
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command is for players.");
            return CommandResult.FAILURE;
        }
        Player player = (Player) sender;

        Player targetPlayer = Bukkit.getPlayer(args[1]);
        if (targetPlayer == null) {
            sender.sendMessage(ChatColor.RED + args[1] + " is not a valid player.");
            return CommandResult.FAILURE;
        }

        Stonks.plugin.configManager.ADMIN_STOCK_LIST.generate(PlayerData.get(targetPlayer),player).open();
        return CommandResult.SUCCESS;

    }
}
